import React from 'react';

import { Header } from './../../_components/header';

class HomeLayout extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <Header />
                {this.props.children}
            </div>
        )
    }
}
export { HomeLayout };
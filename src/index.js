import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

// Import Snackbar
import { SnackbarProvider } from 'notistack';
import { CommonSnackbarConfig } from "./_helpers/snackbar"
import './_assets/css/style.css';
import './_assets/css/common.css';
import './_assets/css/bootstrap.min.css';
import './_assets/css/font-awesome.min.css';
import './_assets/css/elegant-icons.css';
import './_assets/css/nice-select.css';

ReactDOM.render(
    <SnackbarProvider 
        maxSnack={3}
        autoHideDuration={2000}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      >
        <CommonSnackbarConfig />
        <App />
      </SnackbarProvider>,
    document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

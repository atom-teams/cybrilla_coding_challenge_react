import React from 'react';
import { withSnackbar } from 'notistack';

/**
 * Variable Declaration
 */
var useSnackbarRef = null
const setUseSnackbarRef = (props) => {
    useSnackbarRef = props
}

/**
 * Inner Common Snackbar Configurator Componenet Declaration
 */
class InnerCommonSnackbarConfig extends React.Component {
    constructor(props) { super(props); }
    componentDidMount(){ this.props.setUseSnackbarRef(this.props) }
    render() { return null }
}

/**
 * Export ICSC compoenent
 */
const ExportICSC = withSnackbar(InnerCommonSnackbarConfig)

/**
 * Export Methods
 */
export const CommonSnackbarConfig = () => {
    return <ExportICSC setUseSnackbarRef={setUseSnackbarRef} />
}
export default {
    success(msg) { this.toast(msg, 'success') },
    warning(msg) { this.toast(msg, 'warning') },
    info(msg) { this.toast(msg, 'info') },
    error(msg) { this.toast(msg, 'error') },
    toast(msg, variant = 'default') { useSnackbarRef.enqueueSnackbar(msg, { variant }) }
}
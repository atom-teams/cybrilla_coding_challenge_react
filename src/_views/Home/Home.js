import React from "react";
import { envConfig } from "./../../_config/config";
import * as ServiceConst from "./../../_config/api";
import { restServices } from "./../../_services/api-services"
import SnackbarUtils from "./../../_helpers/snackbar"

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            list: []
        }
    }

    componentDidMount() {
        this.getAllProducts()
    }

    getAllProducts = () => {
        let url = envConfig.BASE_API + ServiceConst.PRODUCT_GET_ALL
        this.setState({ isLoading: true });
        restServices.getRequest(
            url,
            (response) => {
              if (response) {
                  this.setState({
                      list: response.data,
                      isLoading: false
                  });
              }
            },
            (error) => {
                this.setState({
                    list: [],
                    isLoading: false
                });
            }
        );
    }

    cartAdd = (prodId) => {
        this.setState({ isLoading: true });
        let url = envConfig.BASE_API + ServiceConst.CART_ADD
        let data = {
            "product_id": prodId,
            "quantity": 1
        };
        restServices.postRequest(
            url,
            data,
            (response) => {
                if (response) {
                    this.setState({ isLoading: false });
                    SnackbarUtils.success("The product has been added successfully")
                }
            },
            (error) => {
                this.setState({ isLoading: false });
                SnackbarUtils.error("Something went wrong")
            }
        )
    }

    render() {

        const { 
            list,
            isLoading
        } = this.state;

        return (
            <div class="main-block container">
                <div class="filter__item">
                    <div class="row">
                        <div class="col-lg-4 col-md-5">
                            <div class="filter__sort">
                                <span>Sort By</span>
                                <select>
                                    <option value="0">Default</option>
                                    <option value="0">Default</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="filter__found">
                                <h6><span>{list.length}</span> Products found</h6>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-3">
                            <div class="filter__option">
                                <span class="icon_grid-2x2"></span>
                                <span class="icon_ul"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    {
                        isLoading == false && list.map((product, index) => (
                            <div class="col-lg-4 col-md-6 col-sm-6" key={index}>
                                <div class="product__item">
                                    <div class="product__item__pic set-bg">
                                        <ul class="product__item__pic__hover">
                                            <li><a onClick={() => this.cartAdd(product._id)}><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="product__item__text">
                                        <h6><a href="#">{product.name}</a></h6>
                                        <h5>{parseFloat(product.price.$numberDecimal).toFixed(2)}</h5>
                                    </div>
                                </div>
                            </div>
                        ))
                    }
                    {
                        isLoading == false && list.length == 0 && (
                            <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                                <p>No Products Available</p>
                            </div>
                        )
                    }
                    {
                        isLoading == true && (
                            <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                                <p>Loading..</p>
                            </div>
                        )
                    }
                </div>
            </div>
        )
    }
}

export { Home };
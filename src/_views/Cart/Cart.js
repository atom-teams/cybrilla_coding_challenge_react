
import React from "react";
import { envConfig } from "./../../_config/config";
import * as ServiceConst from "./../../_config/api";
import { restServices } from "./../../_services/api-services"
import SnackbarUtils from "./../../_helpers/snackbar"

class Cart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            cartData: {}
        }
    }

    componentDidMount() {
        this.getAllCartProducts()
    }

    getAllCartProducts = () => {
        let url = envConfig.BASE_API + ServiceConst.CART_GET_ALL;
        this.setState({ isLoading: true });
        restServices.getRequest(
            url,
            (response) => {
              if (response) {
                  console.log(response.data);
                  this.setState({
                    cartData: response.data,
                      isLoading: false
                  });
              }
            },
            (error) => {
                this.setState({
                    cartData: {},
                    isLoading: false
                });
            }
        );
    }

    cartUpdate = (index) => {
        this.setState({ isLoading: true });
        const { cartData } = this.state;
        let url = envConfig.BASE_API + ServiceConst.CART_ADD
        let data = {
            "product_id": cartData.list[index].product_id,
            "quantity": cartData.list[index].quantity
        };
        restServices.postRequest(
            url,
            data,
            (response) => {
                if (response) {
                    SnackbarUtils.success("cart has been updated successfully")
                    this.setState({ isLoading: false });
                    this.getAllCartProducts()
                }
            },
            (error) => {
                this.setState({ isLoading: false });
                SnackbarUtils.error("Something went wrong")
            }
        )
    }

    cartDelete = (index) => {
        this.setState({ isLoading: true });
        const { cartData } = this.state;
        let url = envConfig.BASE_API + ServiceConst.CART_DELETE + "/" + cartData.list[index]._id
        restServices.deleteRequest(
            url,
            (response) => {
                if (response) {
                    SnackbarUtils.success("cart has been updated successfully")
                    this.setState({ isLoading: false });
                    this.getAllCartProducts()
                }
            },
            (error) => {
                this.setState({ isLoading: false });
                SnackbarUtils.error("Something went wrong")
            }
        )
    }

    // Quantity update while change value
    handleChange = (event, index) => {
        const { cartData } = this.state;
        const { value } = event.target;
        cartData.list[index].quantity = value;
        this.setState({
            cartData
        });
    };

    render() {

        const { 
            cartData,
            isLoading
        } = this.state;

        return (
            <div class="cart-main-block">
                <section class="shoping-cart spad">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="shoping__cart__table">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th class="shoping__product">Products</th>
                                                <th>Price</th>
                                                <th>Quantity</th>
                                                <th>Discount</th>
                                                <th>Total</th>
                                                <th>Action</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                isLoading == true && (
                                                    <tr class="text-center">
                                                        <td colspan="7">Loading...</td>
                                                    </tr>
                                                )
                                            }
                                            {
                                                cartData.list && cartData.list.length > 0 && (
                                                    cartData.list.map((product, index) => (
                                                        <tr>
                                                            <td class="shoping__cart__item">
                                                                <img src="https://www.roadrunnerrecords.com/sites/g/files/g2000005056/f/sample-4.jpg" width="100" alt="" />
                                                                <h5>{product.prodDetail.name}</h5>
                                                                {
                                                                    product.validPromos && product.validPromos.length > 0 && (
                                                                        product.validPromos.map((promo) => (
                                                                            <p><strong>{promo.name} => Discount { promo.d_type == 1 ? "Percentage" : "Price" } { parseFloat(promo.d_value.$numberDecimal).toFixed(2) }</strong></p>
                                                                        ))
                                                                    )
                                                                }
                                                            </td>
                                                            <td class="shoping__cart__price">
                                                                {parseFloat(product.prodDetail.price.$numberDecimal).toFixed(2)}
                                                            </td>
                                                            <td class="shoping__cart__quantity">
                                                                <input type="number" value={product.quantity} min={1} onChange={(e) => this.handleChange(e, index)} />
                                                            </td>
                                                            <td class="shoping__cart__total">
                                                                {parseFloat(product.discount_total).toFixed(2)}
                                                            </td>
                                                            <td class="shoping__cart__total">
                                                                {parseFloat(product.total).toFixed(2)}
                                                            </td>
                                                            <td class="shoping__cart__total">
                                                                <button type="button" class="btn btn-primary" onClick={() => this.cartUpdate(index)} >Update</button>
                                                            </td>
                                                            <td class="shoping__cart__item__close">
                                                                <span class="icon_close"  onClick={() => this.cartDelete(index)}></span>
                                                            </td>
                                                        </tr>
                                                    ))
                                                )
                                            }
                                            {
                                                cartData.list && cartData.list.length == 0 && (
                                                    <tr class="text-center">
                                                        <td colspan="7">No Product added.</td>
                                                    </tr>
                                                )
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="shoping__continue">
                                    <div class="shoping__discount">
                                        <h5>Basket Discount:</h5>
                                        {
                                            cartData.basketPromotions && cartData.basketPromotions.length > 0 && (
                                                cartData.basketPromotions.map((promo) => (
                                                    <p><strong>{promo.name} => Discount { promo.d_type == 1 ? "Percentage" : "Price" } { parseFloat(promo.d_value.$numberDecimal).toFixed(2) }</strong></p>
                                                ))
                                            )
                                        }
                                    </div>
                                </div>
                            </div>
                            {
                                cartData.list && cartData.list.length > 0 && (
                                    <div class="col-lg-6">
                                        <div class="shoping__checkout">
                                            <h5>Cart Total</h5>
                                            <ul>
                                                <li>Subtotal <span>{parseFloat(cartData.cart.sub_total).toFixed(2)}</span></li>
                                                <li>Discount <span>{parseFloat(cartData.cart.discount_total).toFixed(2)}</span></li>
                                                <li>Total <span>{parseFloat(cartData.cart.grand_total).toFixed(2)}</span></li>
                                            </ul>
                                            <a class="primary-btn">PROCEED TO CHECKOUT</a>
                                        </div>
                                    </div>
                                )
                            }
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export { Cart };
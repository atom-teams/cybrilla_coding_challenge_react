
import SnackbarUtils from "./../_helpers/snackbar";

/**
 * Get Rest API Request Headers
 */
function getHeaders() {
    var headers = {};
    headers["Content-Type"] = "application/json";
    return headers;
}

/**
 * Check the response is valid or not
 */
function checkValidJSONReponse(response) {
    if (response.status >= 200 && response.status <= 300) {
        return response.json();
    } else {
        SnackbarUtils.error("Something went wrong")
    }
}

export const restServices = {

    /**
     * Get Request with valid values and callbacks to get responses
     * @param {*} URL 
     * @param {*} successBlock 
     * @param {*} failBlock 
     */
    getRequest(
        URL,
        successBlock,
        failBlock
    ) {

        let requestOptions = {
            method: "GET",
            headers: getHeaders()
        };

        fetch(URL, requestOptions)
        .then(response => {
            return checkValidJSONReponse(response);
        })
        .then(result => {
            if (result) {
                successBlock(result);
            } else {
                if(failBlock) failBlock("Something Went Wrong");
            }
        })
        .catch(error => {
            console.log(error);
            if(failBlock) failBlock(error);
        });
    },

     /**
     * Post request with valid values and callbacks to get responses
     * @param {*} URL 
     * @param {*} data 
     * @param {*} successBlock 
     * @param {*} failBlock 
     */
    postRequest(
        URL,
        data,
        successBlock,
        failBlock
    ) {

        let requestOptions = {
            method: "POST",
            headers: getHeaders(),
            body: JSON.stringify(data)
        };

        fetch(URL, requestOptions)
        .then(response => {
            return response.status === 200 ? response : null;
        })
        .then(result => {
            if (result) {
                successBlock(result);
            } else {
                failBlock("Something Went Wrong");
            }
        })
        .catch(error => {
            failBlock(error);
        });

    },

    /**
     * Delete request with valid values and callbacks to get responses
     * @param {*} URL 
     * @param {*} successBlock 
     * @param {*} failBlock 
     */
    deleteRequest(
        URL,        
        successBlock,
        failBlock
    ) {
        let requestOptions = {
            method: "DELETE",
            headers: getHeaders()
        };

        fetch(URL, requestOptions)
        .then(response => {            
            return (response.status === 200 || response.status === 204) ? response : null;
        })
        .then(result => {
            if (result) {
                successBlock(result);
            } else {
                failBlock("Something Went Wrong");
            }
        })
        .catch(error => {
            failBlock(error);
        });

    }

}

// Products
export const PRODUCT_GET_ALL = 'products';

// Cart
export const CART_ADD = 'cart/add';
export const CART_UPDATE = 'cart/update';
export const CART_GET_ALL = 'cart';
export const CART_DELETE = 'cart/delete';

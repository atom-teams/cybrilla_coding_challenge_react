import React from 'react';
import { Route } from 'react-router-dom';
import { HomeLayout } from './../_layouts/home/home';

//Admin Layout
export const LayoutRoute = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={matchProps => (
            <HomeLayout>
                <Component {...matchProps} />
            </HomeLayout>
        )} />
    )
};
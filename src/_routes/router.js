import React from "react";
import { Router, Switch } from "react-router-dom";
import { history } from "./../_helpers";
import { LayoutRoute } from "./layout";

// Import views componenets
import {
  Home,
  Cart
} from "./../_views";

class RouterComponent extends React.Component {
    render() {
      return (
        <Router history={history}>
          <Switch>
            <LayoutRoute exact path={"/"} component={Home} />
            <LayoutRoute exact path={"/cart"} component={Cart} />
          </Switch>
        </Router>
      );
    }
}
  
export  { RouterComponent };
import React from "react";

class Header extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>
                <header class="header">
                    <div class="header__top">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="header__top__left">
                                        <ul>
                                            <li><i class="fa fa-envelope"></i> hello@cybrilla.com</li>
                                            <li>Free Shipping for all Order</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="header__top__right">
                                        <div class="header__top__right__social">
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-linkedin"></i></a>
                                            <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                        </div>
                                        <div class="header__top__right__language">
                                            <img src="img/language.png" alt="" />
                                            <div>English</div>
                                            <span class="arrow_carrot-down"></span>
                                            <ul>
                                                <li><a href="#">Spanis</a></li>
                                                <li><a href="#">English</a></li>
                                            </ul>
                                        </div>
                                        <div class="header__top__right__auth">
                                            <a href="#"><i class="fa fa-user"></i> Login</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="header__logo">
                                    <a href="./index.html"><img src="img/logo.png" alt="" /></a>
                                </div>
                            </div>
                            <div class="col-lg-6 text-center">
                                <nav class="header__menu">
                                    <ul>
                                        <li><a href="/">Home</a></li>
                                        <li><a href="cart">Cart</a></li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="col-lg-3">
                                &nbsp;
                            </div>
                        </div>
                        <div class="humberger__open">
                            <i class="fa fa-bars"></i>
                        </div>
                    </div>
                </header>
            </div>
        );
    }
}

export { Header };